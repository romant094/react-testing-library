import { useState, useEffect } from 'react';
import { News } from './News';
import { Modal } from './Modal';
import { BrowserRouter } from 'react-router-dom';
import { RouterComponent } from './Router';
import { Counter } from './Counter';

const getUser = () => Promise.resolve({ id: 1, name: 'Tony Stark' });

export const Search = ({ value, onChange, label }) => (
  <div>
    {label && <label htmlFor="search">{label}</label>}
    <input
      id="search"
      type="text"
      value={value}
      onChange={onChange}
      autoComplete="off"
      placeholder="search text..."
    />
  </div>
);

function App () {
  const [search, setSearch] = useState('');
  const [user, setUser] = useState(null);
  const [show, setShow] = useState(false);
  const onSearch = e => setSearch(e.target.value);

  useEffect(() => {
    (async () => {
      setUser(await getUser())
    })()
  }, [])

  return (
    <div className="App">
      {user && <h2>Logged in as {user.name}</h2>}
      <img src="" alt="search image" />
      <Search value={search} onChange={onSearch} label="Search:" />
      <p>Searches for {search || '...'}</p>
      <News />
      <button onClick={() => setShow(true)}>Open modal</button>
      {show && (
        <Modal onClose={() => setShow(false)}>
          <h4>Modal in portal</h4>
          <p>
            Lorem ipsum dolor sit amet, consectetur adipisicing elit. Adipisci aliquam aspernatur deleniti doloribus
            ea eligendi eveniet exercitationem fugit id illo in maxime modi officiis quae quibusdam quidem, rem sint
            veniam!
          </p>
        </Modal>
      )}
      <BrowserRouter>
        <RouterComponent />
      </BrowserRouter>
      <Counter />
    </div>
  );
}

export default App;

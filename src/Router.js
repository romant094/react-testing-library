import { useParams, Link, Switch, Route, withRouter } from 'react-router-dom';

export const Home = () => <h1>Home page</h1>;
export const About = () => <h1>About page</h1>;
export const Error = () => <h1>Error page</h1>;

export const Contact = () => {
  const { name } = useParams();
  return <h1>Welcome, {name}</h1>;
};

export const LocationDisplay = withRouter(({ location }) => (
  <div>Location: {location.pathname}</div>
));

export const RouterComponent = () => (
  <>
    <nav role="navigation">
      <Link to='/'>Home</Link>
      <Link to='/about'>About</Link>
      <Link to='/contact/John'>Contact John</Link>
    </nav>

    <Switch>
      <Route exact path='/' component={Home} />
      <Route exact path='/about' component={About} />
      <Route exact path='/contact/:name' component={Contact} />
      <Route component={Error} />
    </Switch>

    <LocationDisplay />
  </>
)

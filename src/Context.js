import { createContext, useState, useContext } from 'react';

const AuthContext = createContext();

export const AuthProvider = ({ children }) => {
  const [isAuth, setIsAuth] = useState(false);

  const toggleLogin = () => setIsAuth(prev => !prev);

  return (
    <AuthContext.Provider value={{ isAuth, toggleLogin }}>
      {children}
    </AuthContext.Provider>
  );
};

export const Auth = () => {
  const { isAuth, toggleLogin } = useContext(AuthContext);

  return (
    <div className="auth">
      <input
        type="button"
        value="login"
        onClick={toggleLogin}
      />
      {isAuth ? 'Welcome' : 'Please, log in'}
    </div>
  );
};

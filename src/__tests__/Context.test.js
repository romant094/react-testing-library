import { render } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import { AuthProvider, Auth } from '../Context';

describe('Context', () => {
  it('Auth should show default value', () => {
    const { getByText } = render(
      <AuthProvider>
        <Auth />
      </AuthProvider>
    )
    expect(getByText('Please, log in')).toBeInTheDocument()
  });

  it('Auth should toggle value', () => {
    const { getByRole, getByText } = render(
      <AuthProvider>
        <Auth />
      </AuthProvider>
    )
    expect(getByText('Please, log in')).toBeInTheDocument()
    userEvent.click(getByRole('button'))
    expect(getByText('Welcome')).toBeInTheDocument()
    userEvent.click(getByRole('button'))
    expect(getByText('Please, log in')).toBeInTheDocument()
  });
})

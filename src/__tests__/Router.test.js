import { RouterComponent } from '../Router';
import { renderWithRouter } from '../helpers';

describe('Router', () => {
  it('should render the home page', () => {
    const { container } = renderWithRouter(<RouterComponent />);
    const navbar = container.querySelector('nav');
    const links = container.querySelectorAll('a');
    expect(container.innerHTML).toMatch('Home page');
    expect(links.length).toBe(3);
    expect(navbar).toContainElement(links[0]);
  });

  it('should navigate to page with param', () => {
    const { container } = renderWithRouter(<RouterComponent />, { route: '/contact/John' });
    expect(container.innerHTML).toMatch('Welcome, John');
  });

  it('should render 404 page', () => {
    const { container } = renderWithRouter(<RouterComponent />, { route: '/unknown-page' });
    expect(container.innerHTML).toMatch('Error page');
  });

  it('should render a component with current location', () => {
    const route = '/some-page';
    const { getByText } = renderWithRouter(<RouterComponent />, { route });
    expect(getByText(/Location:/)).toHaveTextContent(route);
  });
});

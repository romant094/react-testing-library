import userEvent from '@testing-library/user-event';
import { Counter } from '../Counter';
import { renderWithRedux } from '../helpers';

describe('Redux', () => {
  it('should have initial value 0', () => {
    const { getByRole } = renderWithRedux(<Counter />);
    expect(getByRole('heading')).toHaveTextContent('0');
  });

  it('should increment value', () => {
    const {
      getByRole, getByText
    } = renderWithRedux(<Counter />, { initialState: { count: 5 } });
    userEvent.click(getByText('+1'));
    expect(getByRole('heading')).toHaveTextContent('6');
  });

  it('should decrement value', () => {
    const {
      getByRole, getByText
    } = renderWithRedux(<Counter />, { initialState: { count: 100 } });
    userEvent.click(getByText('-1'));
    expect(getByRole('heading')).toHaveTextContent('99');
  });
});

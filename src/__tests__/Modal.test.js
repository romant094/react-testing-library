import { render } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import { Modal } from '../Modal';

describe('Portal', () => {
  it('modal shows the children and a close button', () => {
    const close = jest.fn();
    const { getByText } = render(
      <Modal onClose={close}>
        My modal
      </Modal>
    );
    expect(getByText('My modal')).toBeInTheDocument();
    userEvent.click(getByText(/close/i));
    expect(close).toHaveBeenCalledTimes(1);
  });

  it('should be unmounted', () => {
    const { getByText, unmount, queryByText } = render(
      <Modal>
        My modal
      </Modal>
    );
    expect(getByText('My modal')).toBeInTheDocument();
    unmount();
    expect(queryByText('My modal')).not.toBeInTheDocument();
  });
});

import { render } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import axios from 'axios';
import { act } from 'react-dom/test-utils';
import { News } from '../News';

jest.mock('axios');
const hits = [
  { objectID: '1', title: 'React 1' },
  { objectID: '2', title: 'React 2' }
];

describe('News', () => {
  it('fetches data from api', async () => {
    axios.get.mockImplementationOnce(() => Promise.resolve({ data: { hits } }));
    const { getByRole, findAllByRole, findByText, getByText } = render(<News />);
    userEvent.click(getByRole('button'))
    const items = await findAllByRole('listitem')
    expect(items).toHaveLength(2)
    // alternative
    expect(axios.get).toHaveBeenCalledTimes(1)
    expect(axios.get).toHaveBeenCalledWith('http://hn.algolia.com/api/v1/search?query=react')
  });

  it('fetches data from api and gets error', async () => {
    axios.get.mockImplementationOnce(() => Promise.reject(new Error()))
    const { getByRole, findByText } = render(<News />);
    userEvent.click(getByRole('button'))
    const message = await findByText('Something is wrong...')
    expect(message).toBeInTheDocument()
  });

  it('fetches data from api #alternative ', async () => {
    const promise = Promise.resolve({ data: { hits } })
    axios.get.mockImplementationOnce(() => promise);
    const { getByRole, getAllByRole } = render(<News />);
    userEvent.click(getByRole('button'))
    await act(() => promise)
    expect(getAllByRole('listitem')).toHaveLength(2)
  });
});

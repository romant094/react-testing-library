import React from "react";
import { Provider } from 'react-redux';
import ReactDOM from "react-dom";
import App from '../App';
import { store } from '../redux/store';

jest.mock("react-dom", () => ({ render: jest.fn() }));

const appRoot = (
  <React.StrictMode>
    <Provider store={store}>
      <App />
    </Provider>
  </React.StrictMode>
)

describe("Application root", () => {
  it("should render without crashing", () => {
    const div = document.createElement("div");
    div.id = "root";
    document.body.appendChild(div);
    require("../index.js");
    expect(ReactDOM.render).toHaveBeenCalledWith(appRoot, div);
  });
});

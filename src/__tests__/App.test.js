import { render } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import App from '../App';
import { act } from 'react-dom/test-utils';
import { renderWithRedux } from '../helpers';

describe('App', () => {
  it('renders App component', async () => {
    const { findByText, queryByText, getByRole } = renderWithRedux(<App />);
    await findByText(/Logged in as/i);
    expect(queryByText(/Searches for React/i)).toBeNull();
    // fireEvent.change(screen.getByRole('textbox'), {
    //   target: { value: 'React' }
    // });
    userEvent.type(getByRole('textbox'), 'React');
    expect(queryByText(/Searches for React/i)).toBeInTheDocument()
  });

  it('should open modal via button', async () => {
    await act(async () => {
      const { getByText, findByText, baseElement } = await renderWithRedux(<App />)
      userEvent.click(getByText('Open modal'));
      expect(await findByText(/Modal in portal/)).toBeInTheDocument()
    })
  });
});

describe('events', () => {
  it('checkbox click', () => {
    const handleChange = jest.fn();
    const { container } = render(<input
      type="checkbox"
      onChange={handleChange}
    />);
    const checkbox = container.firstChild;
    expect(checkbox).not.toBeChecked();
    // fireEvent.click(checkbox);
    userEvent.click(checkbox);
    // userEvent.click(checkbox, { ctrlKey: true, shiftKey: true }); // if need to check pressed keys
    expect(checkbox).toBeChecked();
    // expect(handleChange).toBeCalledTimes(1) // alternative
  });

  it('double click', () => {
    const handleChange = jest.fn();
    const { container } = render(<input
      type="checkbox"
      onChange={handleChange}
    />);
    const checkbox = container.firstChild;
    expect(checkbox).not.toBeChecked();
    userEvent.dblClick(checkbox);
    expect(handleChange).toBeCalledTimes(2);
  });

  it('input focus', () => {
    const { getByTestId } = render(<input
      type="text"
      data-testid="dummy-input"
    />);
    const input = getByTestId('dummy-input');
    expect(input).not.toHaveFocus();
    input.focus();
    expect(input).toHaveFocus();
  });

  it('focus via tab', () => {
    const { getAllByTestId } = render(
      <div>
        <input
          data-testid="dummy-input"
          type="checkbox"
        />
        <input
          data-testid="dummy-input"
          type="radio"
        />
        <input
          data-testid="dummy-input"
          type="number"
        />
      </div>
    );
    const [checkbox, radio, number] = getAllByTestId('dummy-input');
    userEvent.tab();
    expect(checkbox).toHaveFocus();
    userEvent.tab();
    expect(radio).toHaveFocus();
    userEvent.tab();
    expect(number).toHaveFocus();
  });

  it('select option ', () => {
    const { selectOptions, getByRole, getByText } = render(
      <select>
        <option value="1">A</option>
        <option value="2">B</option>
        <option value="3">C</option>
      </select>
    );
    userEvent.selectOptions(getByRole('combobox'), '1');
    expect(getByText('A').selected).toBeTruthy();
    userEvent.selectOptions(getByRole('combobox'), '2');
    expect(getByText('A').selected).toBeFalsy();
    expect(getByText('B').selected).toBeTruthy();
  });
});

import React, { useState } from 'react';
import axios from 'axios';

const url = 'http://hn.algolia.com/api/v1/search';

export const News = () => {
  const [news, setNews] = useState([]);
  const [error, setError] = useState(null);

  const onClick = async () => {
    if (error) {
      setError(null);
    }
    try {
      const res = await axios.get(`${url}?query=react`);
      setNews(res?.data?.hits || []);
    } catch (err) {
      setError(err);
    }
  };

  return (
    <div style={{ padding: '20px 0' }}>
      <button onClick={onClick}>Fetch news</button>
      {error && <div>Something is wrong...</div>}
      {!!news.length && (
        <ul className="news">
          {news.map(({ objectID, url, title }) => (
            <li key={objectID}>
              <a href={url} target="_blank" rel="noreferrer nofollow">{title}</a>
            </li>
          ))}
        </ul>
      )}
    </div>
  );
};

import { TYPES } from './types';

export const increment = () => ({
  type: TYPES.INCREMENT
})

export const decrement = () => ({
  type: TYPES.DECREMENT
})

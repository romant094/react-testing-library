import { TYPES } from './types';

export const initialState = {
  count: 0
};

export const reducer = (state = initialState, action) => {
  switch (action.type) {
    case TYPES.INCREMENT:
      return {
        count: state.count + 1
      };
    case TYPES.DECREMENT:
      return {
        count: state.count - 1
      };
    default:
      return state;
  }
};

import React, { useEffect } from 'react';
import { createPortal } from 'react-dom';

const root = document.createElement('div');
root.setAttribute('id', 'modal-root');
document.body.appendChild(root);

export const Modal = ({ onClose, children }) => {
  const el = document.createElement('div');

  useEffect(() => {
    root.appendChild(el);
    return () => root.removeChild(el);
  });

  return createPortal(
    <div
      onClick={e => e.stopPropagation()}
      style={{ padding: '10px 50px' }}
    >
      {children}
      <button onClick={onClose}>Close</button>
    </div>,
    el
  );
};

import React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import * as actions from './redux/actions'

export const Counter = () => {
  const dispatch = useDispatch();
  const { count } = useSelector(state => state)
  const increment = () => dispatch(actions.increment())
  const decrement = () => dispatch(actions.decrement())

  return (
    <div>
      <h1>{count}</h1>
      <button onClick={decrement}>-1</button>
      <button onClick={increment}>+1</button>
    </div>
  );
};
